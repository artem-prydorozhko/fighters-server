const { saveData, getData, getDataId, deleteData, updateData  } = require("../repositories/user.repository");

const getHeroes = () => {
    return getData();
}

const getHero = (id) => {
  if(id) {
    return getDataId(id);
  } else {
    return null;
  }
}

const deleteHero = (id) => {
  if(id) {
    return deleteData(id);
  } else {
    return null;
  }
}

const updateHero = (data) => {
  if(data) {
    return updateData(data);
  } else {
    return null;
  }
}

const saveHero = (hero) => {
    if(hero) {
        return saveData(hero);
    } else {
        return null;
    }
}

module.exports = {
  getHeroes,
  getHero,
  saveHero,
  deleteHero,
  updateHero,
};