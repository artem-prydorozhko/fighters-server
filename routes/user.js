
const express = require('express');
const router = express.Router();

const { saveHero, getHeroes, getHero, deleteHero, updateHero } = require("../services/user.service");

const { isAuthorized } = require("../middlewares/auth.middleware");

// if in url there is an id parametr then response is one hero otherwise all heros
router.get('/', function(req, res, next) {
  try {
    if(req.query.id) {
      const result = getHero(req.query.id);
      if(!result)
        res.status(400).send('bad id');
      else
        res.send(result);
    } 
    else {
      const result = getHeroes();
      res.send(result);
    }
  } catch(err) {
    console.error(err);
  }
});

router.delete('/', function(req, res, next) {
  try {
    if(req.query.id) {
      const result = deleteHero(req.query.id);
      if(!result)
        res.status(400).send('bad id');
      else
        res.send(result);
    } 
    else {
      res.send('bad id');
    }
  } catch(err) {
    console.error(err);
  }
});

router.post('/', function(req, res, next) {
  const result = saveHero(req.body);

  if (result) {
    res.send(result);
  } else {
    res.status(500).send(`server error`);
  }
});

router.put('/', function(req, res, next) {
  if(req.body) {
    const result = updateHero(req.body);
    if(!result)
      res.status(400).send('bad request');
    else
      res.send(result);
  } 
  else {
    res.send('bad request');
  }
});

module.exports = router;
