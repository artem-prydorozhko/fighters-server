const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Read readme.md file to know how to use API');
});

module.exports = router;
