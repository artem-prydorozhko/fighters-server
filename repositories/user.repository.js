const fs = require('fs');


const getData = () => {
    const heroes = fs.readFileSync('heroes.json');
    return heroes;
}

const getDataId =(id) => {
    const heroes = fs.readFileSync('heroes.json');
    const result = JSON.parse(heroes);
    if(id > result.length || id <= 0) 
        return null;
    return JSON.stringify(result[id-1],null,4);
}

const saveData = (data) => {
    if (data) {
      if(data._id <= 0) {
        return null;
      }
      const heroes = fs.readFileSync('heroes.json');
      const result = JSON.parse(heroes);
      for(let hero of result) {
          if(hero._id == data._id)
            return 'id already exists';
      }
      result.push(data);
      fs.writeFileSync('heroes.json', JSON.stringify(result,null,4), {flag: 'w'});
      return 'success';
    } else {
      return null;
    }
}

const deleteData = (id) => {
    if (id && id > 0) {
        const heroes = fs.readFileSync('heroes.json');
        const result = JSON.parse(heroes);
        let deleted = false;
        for(let i = 0, n = result.length; i < n; i++) {
            if(result[i]._id == id) {
                result.splice(i,1);
                deleted = true;
                break;
            }
        }

        if(!deleted) {
            return null;
        }

        fs.writeFileSync('heroes.json', JSON.stringify(result,null,4), {flag: 'w'});
        return 'success';
      } else {
        return null;
      }
}

const updateData = (data) => {
    if (data && data._id > 0) {
        const heroes = fs.readFileSync('heroes.json');
        const result = JSON.parse(heroes);
        let updated = false;
        for(let i = 0, n = result.length; i < n; i++) {
            if(result[i]._id == data._id) {
                result[i].name = data.name?data.name:result[i].name;
                result[i].health = data.health?data.health:result[i].health;
                result[i].attack =data.attack?data.attack:result[i].attack;
                result[i].defense = data.defense?data.defense:result[i].defense;
                result[i].source = data.source?data.source:result[i].source;
                updated = true;
                break;
            }
        }

        if(!updated) {
            return null;
        }

        fs.writeFileSync('heroes.json', JSON.stringify(result,null,4), {flag: 'w'});
        return 'success';
      } else {
        return null;
      }
}
  
 module.exports = {
    saveData,
    getData,
    getDataId,
    deleteData,
    updateData,
  };