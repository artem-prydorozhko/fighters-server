# street-fighters-server

>server for street fighters browser game

[server-repository](https://bitbucket.org/artem-prydorozhko/fighters-server/src/master/)

## deployed
        
 [heroku](https://street-fighters-server.herokuapp.com)

## game version on branch develop

[game-repository](https://bitbucket.org/artem-prydorozhko/street-fighters/src/develop/)

## API

+ GET: /user

   получение массива всех пользователей

+ GET: /user?id=

   получение одного пользователя по ID

+ POST: /user

   создание пользователя по данным передаваемым в теле запроса (content-type: application/json)

+ PUT: /user

   обновление пользователя по данным передаваемым в теле запроса. Обязательно должно быть поле id (content-type: application/json)

+ DELETE: /user?id=

   удаление одного пользователя по ID
    
